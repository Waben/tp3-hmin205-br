package com.example.tp3;

import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import android.os.Bundle;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;

public class Planning extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_planning);
        PlanningModel model=new PlanningModel();
        model.loadActivities();
        ArrayList<String> res = model.getActivities();

        TextView s1 = (TextView) findViewById(R.id.textView);
        TextView s2 = (TextView) findViewById(R.id.textView2);
        TextView s3 = (TextView) findViewById(R.id.textView3);
        TextView s4 = (TextView) findViewById(R.id.textView4);

        s1.setText("08h-10h : "+res.get(0));
        s2.setText("10h-12h : "+res.get(1));
        s3.setText("14h-16h : "+res.get(2));
        s4.setText("16h-18h : "+res.get(3));
    }
}