package com.example.tp3;

import androidx.lifecycle.ViewModel;

import java.util.ArrayList;

public class PlanningModel extends ViewModel {

    private ArrayList<String> activities;
    public ArrayList<String> getActivities(){
        if (activities == null){
            activities = new ArrayList<String>();
            //loadActivities();
        }
        return activities;
    }
    public void loadActivities(){
        activities = new ArrayList<>();
        activities.add("Jouer");
        activities.add("Manger");
        activities.add("Dormir");
        activities.add("Ne pas respecter le couvre-feu");
    }

}
