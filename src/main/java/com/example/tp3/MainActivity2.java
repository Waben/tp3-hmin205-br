package com.example.tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;

import java.util.ArrayList;

import static com.example.tp3.MainActivity.EXTRA_FILE;

public class MainActivity2 extends AppCompatActivity {

    ArrayList<String> data = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();
        String name_file = bundle.get(EXTRA_FILE).toString();

        Writer.readFromFile(this, name_file, this.data);
        TextView name_edit = findViewById(R.id.name);
        TextView surname_edit = findViewById(R.id.lastname);
        TextView age_edit = findViewById(R.id.age);
        TextView number_edit = findViewById(R.id.number);

        String [] info_split = data.get(data.size()-1).split(";");


        name_edit.setText("Name : " +info_split[0]);
        surname_edit.setText("Lastname : "+info_split[1]);
        number_edit.setText("Phone Number : "+info_split[2]);
        age_edit.setText("Age : " + info_split[3]);
    }
}