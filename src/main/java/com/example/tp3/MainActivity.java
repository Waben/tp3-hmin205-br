package com.example.tp3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.util.Random;

public class MainActivity extends AppCompatActivity {
    public  final static String NAME = "name";
    public static final String SURNAME = "surname";
    public static final String PHONE = "number";
    public static final String AGE = "age";
    public static final String ID = "id";
    public static final String EXTRA_FILE = "com.example.tp3.FILE";


    static EditText nameT;
    static EditText lastnameT;
    static EditText nT;
    static EditText ageT;


    static Utilisation util = new Utilisation();

    static int id;
    static boolean firstBool = true;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        nameT = (EditText) findViewById(R.id.name);
        lastnameT = (EditText) findViewById(R.id.lastname);
        ageT = (EditText) findViewById(R.id.age);
        nT = (EditText) findViewById(R.id.number);

        if ((savedInstanceState != null) &&(savedInstanceState.containsKey(NAME))) {
            String name_str = savedInstanceState.getString(NAME);
            nameT.setText(name_str);
        }
        if ((savedInstanceState != null) &&(savedInstanceState.containsKey(SURNAME))) {
            String surname_str = savedInstanceState.getString(SURNAME);
            lastnameT.setText(surname_str);
        }
        if ((savedInstanceState != null) &&(savedInstanceState.containsKey(PHONE))) {
            String number_str = savedInstanceState.getString(PHONE);
            nT.setText(number_str);
        }
        if ((savedInstanceState != null) &&(savedInstanceState.containsKey(AGE))) {
            String age_str = savedInstanceState.getString(AGE);
            ageT.setText(age_str);
        }
        if(firstBool)
        {
            Random r = new Random();
            id = r.nextInt();
            firstBool = false;
        }
        Button button1 = findViewById(R.id.button);
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intentNext = new Intent(MainActivity.this, MainActivity2.class);
                Toast.makeText(getApplicationContext(), "Number of OnResume: " + util.NombreUtilisation(), Toast.LENGTH_LONG).show();
                String data = nameT.getText().toString() + ";" + lastnameT.getText().toString() + ";" + ageT.getText().toString() + ";" + nT.getText().toString();
                String name_file = lastnameT.getText().toString() + id+".sav";
                Writer.writeToFile(name_file, data,MainActivity.this);
                intentNext.putExtra(EXTRA_FILE, name_file);
                startActivity(intentNext);
            }
        });
        Button button2 = findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Planning.class);
                startActivity(intent);
            }
        });


    }
    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(NAME, nameT.getText().toString());
        outState.putString(SURNAME, lastnameT.getText().toString());
        outState.putString(PHONE, nT.getText().toString());
        outState.putString(AGE, ageT.getText().toString());
        outState.putInt(ID,id);
    }

    @Override
    public void onResume() {
        super.onResume();
        util.inc();
    }
}